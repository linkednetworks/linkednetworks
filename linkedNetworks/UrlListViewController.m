//
//  UrlListViewController.m
//  linkedNetworks
//
//  Created by Variable Vasas on 30/01/16.
//  Copyright © 2016 Variable Vasas. All rights reserved.
//

#import "UrlListViewController.h"
#import "Urls.h"
#include <Carbon/Carbon.h>

@interface UrlListViewController ()
- (IBAction)cellClicked:(id)sender;
@property (nonatomic, retain) NSOrderedSet *urlsArray;
@property (weak) IBOutlet NSTableView *urlTableView;

@end

@implementation UrlListViewController
- (id)initWithNibNameCustom:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil context:(NSManagedObjectContext *)context objectModel:(NSManagedObjectModel *)objectModel storeCoordinator:(NSPersistentStoreCoordinator *) storeCoordinator
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        _managedObjectContext = context;
        _managedObjectModel = objectModel;
        _persistentStoreCoordinator = storeCoordinator;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (void) setUser:(User *)user {
    _user = user;
    _urlsArray = [user urls];
}

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification{
    NSInteger selectedRow = self.urlTableView.selectedRow;
    NSString* path = [[NSBundle mainBundle] pathForResource:@"linkedInScript" ofType:@"scpt"];
    if (path != nil)
    {
        NSURL *url = [NSURL fileURLWithPath:path];
        if (url != nil) {
            NSDictionary  *errorDict     = [NSDictionary dictionary];
            NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithContentsOfURL:url
                                                                                 error:&errorDict];
            if (scriptObject != nil) {
                NSString *urlStr = [[_urlsArray objectAtIndex:selectedRow] urlString];
                if ([urlStr rangeOfString:@"http://"].location == NSNotFound && [urlStr rangeOfString:@"https://"].location == NSNotFound) {
                    urlStr =    [NSString stringWithFormat:@"https://%@", urlStr];
                }
                NSAppleEventDescriptor *firstParameter = [NSAppleEventDescriptor descriptorWithString:urlStr];
                // create and populate the list of parameters (in our case just one)
                NSAppleEventDescriptor *parameters = [NSAppleEventDescriptor listDescriptor];
                [parameters insertDescriptor:firstParameter atIndex:1];
                // create the AppleEvent target
                ProcessSerialNumber     psn    = { 0, kCurrentProcess };
                NSAppleEventDescriptor *target = [NSAppleEventDescriptor descriptorWithDescriptorType:typeProcessSerialNumber
                                                                                    bytes:&psn
                                                                                   length:sizeof(ProcessSerialNumber)];
    
    
                // create an NSAppleEventDescriptor with the script's method name to call,
                // this is used for the script statement: "on launch_app(app_name)"
                // Note that the routine name must be in lower case.
                NSAppleEventDescriptor *handler = [NSAppleEventDescriptor descriptorWithString: [@"launch_app" lowercaseString]];
    
                // create the event for an AppleScript subroutine,
                // set the method name and the list of parameters
                NSAppleEventDescriptor *event = [NSAppleEventDescriptor appleEventWithEventClass:kASAppleScriptSuite
                                                                             eventID:kASSubroutineEvent
                                                                    targetDescriptor:target
                                                                            returnID:kAutoGenerateReturnID
                                                                       transactionID:             kAnyTransactionID];
                [event setParamDescriptor:handler forKeyword:keyASSubroutineName];
                [event setParamDescriptor:parameters forKeyword:keyDirectObject];
                // call the event in AppleScript
                if (![scriptObject executeAppleEvent:event error:&errorDict]) {
                    // report any errors from 'errors'
                }
            }
        }
    }
}

- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row {
    
    NSTextField *result = [tableView makeViewWithIdentifier:@"MyView" owner:self];
    if (result == nil) {
        
        // Create the new NSTextField with a frame of the {0,0} with the width of the table.
        // Note that the height of the frame is not really relevant, because the row height will modify the height.
        NSRect frame = NSMakeRect(0, 0, 50, 50);
        result = [[NSTextField alloc] initWithFrame:frame];
        result.editable = NO;
        
        // The identifier of the NSTextField instance is set to MyView.
        // This allows the cell to be reused.
        result.identifier = @"MyView";
    }
    Urls *url = [_urlsArray objectAtIndex:row];
    NSString *text = url.name;
    result.stringValue = text;
    return result;
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return _urlsArray.count;
}

@end
