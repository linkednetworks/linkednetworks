//
//  AppDelegate.m
//  linkedNetworks
//
//  Created by Variable Vasas on 29/12/15.
//  Copyright © 2015 Variable Vasas. All rights reserved.
//
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "AXStatusItemPopup.h"
#import "ContentViewController.h"
#import "UrlListViewController.h"
#import "User.h"
#import "Urls.h"

#define kMinViewWidth 22

@interface AppDelegate ()
    @property (weak) IBOutlet NSWindow *window;
    @property (strong, nonatomic) NSStatusItem *statusItem;
    @property(assign, nonatomic) BOOL active;
@end

@implementation AppDelegate
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    [ self initializePopOver ];
    
}

- (void) initializePopOver
{
    // --------------
    //
    
//    // init content view controller
//    // will be shown inside the popover
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    User *user = [NSEntityDescription
//                                      insertNewObjectForEntityForName:@"User"
//                                      inManagedObjectContext:context];
//    user.email = @"kritivasrocks@gmail.com";
//    user.loggedIn = @1;
//    user.authToken = @"Testland";
//    Urls *urls = [NSEntityDescription
//                                            insertNewObjectForEntityForName:@"Urls"
//                                            inManagedObjectContext:context];
//    //urls.urlString = @"http://google.com";
//    //urls.validTill = [NSDate date];
//    //NSOrderedSet <Urls *> = [Urls alloc];
    NSError *error;
//    if (![context save:&error]) {
//        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
//    }
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"User" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    bool userLoggedIn = false;
    NSString *authToken = nil;
    User *user;
    for (User *info in fetchedObjects) {
        if(![info valueForKey:@"loggedIn"] || true){
            [context deleteObject:info];
        } else {
            userLoggedIn = true;
            authToken = [info valueForKey:@"authToken"];
            user = info;
        }
    }
    
    // init the status item popup
    NSImage *image = [NSImage imageNamed:@"businessman30.png"];
    NSImage *alternateImage = [NSImage imageNamed:@"cloudgrey"];
    UrlListViewController *nextView = [[UrlListViewController alloc] initWithNibNameCustom:@"UrlListViewController" bundle:nil context:__managedObjectContext objectModel:__managedObjectModel storeCoordinator:__persistentStoreCoordinator];
    
    if (userLoggedIn) {
        _statusItemPopup = [[AXStatusItemPopup alloc] initWithViewController:_nextView image:image];
        [_statusItemPopup openUrlListView: nextView];
        
    } else {
        _nextView = [[ContentViewController alloc] initWithNibName:@"ContentViewController" bundle:nil context:__managedObjectContext objectModel:__managedObjectModel storeCoordinator:__persistentStoreCoordinator nextViewPass: nextView];
        _statusItemPopup = [[AXStatusItemPopup alloc] initWithViewController:_nextView image:image];
    }
    
    // globally set animation state (optional, defaults to YES)
    //_statusItemPopup.animated = NO;
    
    //
    // --------------
    // optionally set the popover to the contentview to e.g. hide it from there
    if(userLoggedIn){
        ((UrlListViewController *)_nextView).statusItemPopup = _statusItemPopup;
    } else {
        ((ContentViewController *)_nextView).statusItemPopup = _statusItemPopup;
    }
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __persistentStoreCoordinator;
}
#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end

