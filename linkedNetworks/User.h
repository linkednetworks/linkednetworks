//
//  User.h
//  linkedNetworks
//
//  Created by Variable Vasas on 19/02/16.
//  Copyright © 2016 Variable Vasas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Urls;

NS_ASSUME_NONNULL_BEGIN

@interface User : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "User+CoreDataProperties.h"
