set screenCount to 8
set screenWidth to 1950
global _width
global _height
global windowId
global baseURL
global theIndexOfW
global nextPageLink
global w
set baseURL to ""

on theSplit(theString, theDelimiter)
    try
        set oldDelimiters to AppleScript's text item delimiters
        set AppleScript's text item delimiters to theDelimiter
        set theArray to every text item of theString
        set AppleScript's text item delimiters to oldDelimiters
        return theArray
    on error errStr number errorNumber
        display dialog errStr
    end try
end theSplit

on getNextPage()
    try
        tell application "Google Chrome"
            set w to get window theIndexOfW
            set baseURL to nextPageLink
            set URL of active tab of w to (nextPageLink)
        end tell
        set theRes to my getTheUrlsArray()
        my iterateOverUrls(theRes)
        on error errStr number errorNumber
        #display dialog errStr
    end try
end getNextPage

on createWindow()
    #display dialog "creating window"
    try
        tell application "Google Chrome"
            #display dialog baseURL
            set w to make new window with properties {bounds:{50, 50, _width * (0.9), _height * (0.9)}}
            set visible of w to false
            #tell application "System Events" to keystroke "F" using {command down, shift down}
            set URL of active tab of w to (baseURL)
            set windowId to id of w
            set theIndexOfW to index of w
            
            log "hgjhgjhgjhg"
            #display dialog the_state
            set nextPageLinkStatus to "hasUrl"
            repeat until nextPageLinkStatus is "hasNoUrl"
                #getting urls array
                #display dialog "IN NEXT PAGE LINK"
                if w exists then
                    set the_state to missing value
                    repeat until the_state is "complete"
                        set the_state to (execute active tab of w javascript "if(document.getElementById('results-container') == null) state = 'incomplete'; else state = 'complete';")
                        log the_state
                    end repeat
                    set theScript to "
                        var nodes = document.getElementById('results').childNodes;
                        stringSeparatesUrls = '';
                        for(var i=0;i<nodes.length;i++){
                            var a = nodes[i].getElementsByClassName('result-image')[0];
                            if(a) stringSeparatesUrls = '{=}{=}{=}{=}' + a.href + stringSeparatesUrls;
                        }
                    "
                    #display dialog theScript
                    set theNextPageScript to "
                        var paginated = document.getElementById('results-pagination');
                        var ul = paginated.getElementsByClassName('pagination')[0];
                        var nodes = ul.childNodes;
                        nextPageUrl = 'null';
                        for(var i=0;i<nodes.length;i++){
                            if(nodes[i].getAttribute('class').split('active').length == 2){
                                var a = nodes[i+1].getElementsByTagName('a')[0]
                                if(a && a.href)nextPageUrl = a.href;
                                break;
                            }
                        }
                    "
                    #display dialog theNextPageScript
                    set theRes to (execute active tab of w javascript theScript)
                    set nextPageLink to (execute active tab of w javascript theNextPageScript)
                            if nextPageLink is "null" then
                        set nextPageLinkStatus to "hasNoUrl"
                    end if
                    #split and start iterating
                    if w exists then
                        #display dialog "iterating over urls"
                        set urlsArray to reverse of my theSplit(theRes, "{=}{=}{=}{=}")
                        repeat with theUrl in urlsArray
                            set baseURL to theUrl
                            if w exists then
                                #set URL of active tab of w to (theUrl)
                                execute active tab of w javascript "window.location='" & theUrl & "';"
                                #display dialog theUrl
                            else
                                set w to make new window with properties {bounds:{50, 50, _width * (0.9), _height * (0.9)}}
                                #tell application "System Events" to keystroke "F" using {command down, shift down}
                                set URL of active tab of w to (baseURL)
                                set windowId to id of w
                                set theIndexOfW to index of w
                                log theIndexOfW
                                set the_state to missing value
                                repeat until the_state is "complete"
                                    set the_state to (execute active tab of w javascript "if(document.getElementById('results-container') == null) state = 'incomplete'; else state = 'complete';")
                                    log the_state
                                end repeat
                            end if
                            delay 15
                        end repeat
                    else
                        set w to make new window with properties {bounds:{50, 50, _width * (0.9), _height * (0.9)}}
                        #tell application "System Events" to keystroke "F" using {command down, shift down}
                        set URL of active tab of w to (baseURL)
                        set windowId to id of w
                        set theIndexOfW to index of w
                        log theIndexOfW
                        set the_state to missing value
                        repeat until the_state is "complete"
                            set the_state to (execute active tab of w javascript "if(document.getElementById('results-container') == null) state = 'incomplete'; else state = 'complete';")
                            log the_state
                        end repeat
                    end if
                else
                    set w to make new window with properties {bounds:{50, 50, _width * (0.9), _height * (0.9)}}
                    #tell application "System Events" to keystroke "F" using {command down, shift down}
                    set URL of active tab of w to (baseURL)
                    set windowId to id of w
                    set theIndexOfW to index of w
                    log theIndexOfW
                    log "hgjhgjhgjhg"
                    set the_state to missing value
                    repeat until the_state is "complete"
                        set the_state to (execute active tab of w javascript "if(document.getElementById('results-container') == null) state = 'incomplete'; else state = 'complete';")
                        log the_state
                    end repeat
                end if
                set baseURL to nextPageLink
                #display dialog nextPageLink
                execute active tab of w javascript "window.location='" & nextPageLink & "';"
                #set URL of active tab of w to (nextPageLink)
            end repeat
        end tell
    on error errStr number errorNumber
        display dialog errStr
    end try
end createWindow

on launch_app(baseURLArgv)
    tell application "Finder"
        set _b to bounds of window of desktop
        set _width to item 3 of _b
        set _height to item 4 of _b
    end tell
    #display dialog "in launch app"
    set baseURL to baseURLArgv
    my createWindow()
end launch_app
