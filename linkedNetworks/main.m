//
//  main.m
//  linkedNetworks
//
//  Created by Variable Vasas on 29/12/15.
//  Copyright © 2015 Variable Vasas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
