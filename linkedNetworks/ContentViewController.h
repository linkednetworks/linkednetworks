//
//  ContentViewController.h
//  StatusItemPopup
//
//  Created by Alexander Schuch on 06/03/13.
//  Copyright (c) 2013 Alexander Schuch. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AXStatusItemPopup.h"
#import "UrlListViewController.h"

@interface ContentViewController : NSViewController<NSURLConnectionDelegate>

@property(weak, nonatomic) AXStatusItemPopup *statusItemPopup;
- (IBAction)closeButtonPressed:(id)sender;
+ (NSAppleEventDescriptor *)descriptorWithString:(NSString *)string;
@property (weak) IBOutlet NSSecureTextField *password;
@property (weak) IBOutlet NSTextField *userName;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) UrlListViewController *nextView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil context:(NSManagedObjectContext *)context objectModel:(NSManagedObjectModel *)objectModel storeCoordinator:(NSPersistentStoreCoordinator *) storeCoordinator nextViewPass:(UrlListViewController *)nextViewPass;
@end