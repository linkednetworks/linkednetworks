//
//  UrlListViewController.h
//  linkedNetworks
//
//  Created by Variable Vasas on 30/01/16.
//  Copyright © 2016 Variable Vasas. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "User.h"
#import "AXStatusItemPopup.h"

@interface UrlListViewController : NSViewController<NSURLConnectionDelegate, NSTableViewDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) User *user;
@property (strong, nonatomic) AXStatusItemPopup *statusItemPopup;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
- (id)initWithNibNameCustom:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil context:(NSManagedObjectContext *)context objectModel:(NSManagedObjectModel *)objectModel storeCoordinator:(NSPersistentStoreCoordinator *) storeCoordinator;
- (void) setUser:(User *)user;
@end
