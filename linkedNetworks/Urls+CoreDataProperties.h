//
//  Urls+CoreDataProperties.h
//  linkedNetworks
//
//  Created by Variable Vasas on 03/03/16.
//  Copyright © 2016 Variable Vasas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Urls.h"

NS_ASSUME_NONNULL_BEGIN

@interface Urls (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *urlString;
@property (nullable, nonatomic, retain) NSDate *validTill;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) User *user;

@end

NS_ASSUME_NONNULL_END
