//
//  Urls+CoreDataProperties.m
//  linkedNetworks
//
//  Created by Variable Vasas on 03/03/16.
//  Copyright © 2016 Variable Vasas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Urls+CoreDataProperties.h"

@implementation Urls (CoreDataProperties)

@dynamic urlString;
@dynamic validTill;
@dynamic name;
@dynamic user;

@end
