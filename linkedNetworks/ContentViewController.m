//
//  ContentViewController.m
//  StatusItemPopup
//
//  Created by Alexander Schuch on 06/03/13.
//  Copyright (c) 2013 Alexander Schuch. All rights reserved.
//

#import "ContentViewController.h"
#import "User.h"
#import "Urls.h"
#import "UrlListViewController.h"
#include <Carbon/Carbon.h>

@interface ContentViewController ()
     @property(assign, nonatomic) NSMutableData *responseData;
@end

@implementation ContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil context:(NSManagedObjectContext *)context objectModel:(NSManagedObjectModel *)objectModel storeCoordinator:(NSPersistentStoreCoordinator *) storeCoordinator nextViewPass:(UrlListViewController *)nextViewPass
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        _managedObjectContext = context;
        _managedObjectModel = objectModel;
        _persistentStoreCoordinator = storeCoordinator;
        _nextView = nextViewPass;
    }
    return self;
}

- (IBAction)closeButtonPressed:(id)sender
{
    [_statusItemPopup hidePopover];
}

- (IBAction)startTheLinkedInCrawl:(id)sender
{
    [_userName setStringValue:@"kritivas.shukla@gmail.com"];
    [_password setStringValue:@"@indemica12"];
    if(_userName.stringValue == nil) return;
    if(_password.stringValue == nil) return;
    //NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:3000/users/login"]];
    NSURL *URL = [NSURL URLWithString:@"https://sleepy-crag-69713.herokuapp.com/users/login"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    NSDictionary *dictionary = @{@"email": _userName.stringValue, @"password": _password.stringValue};
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:kNilOptions error:&error];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSDictionary *sessionHeaders = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    @"application/json", @"Accept", @"application/json",  @"Content-Type", nil];
    config.HTTPAdditionalHeaders = sessionHeaders;
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data completionHandler:^(NSData *data,NSURLResponse *response,NSError *error)
    {
        //[_statusItemPopup hidePopover];
        NSLog(@"dataAsString %@", [NSString stringWithUTF8String:[data bytes]]);
        NSString* urlStr = [NSString stringWithUTF8String:[data bytes]];
        NSError *error1;
        NSMutableDictionary * innerJson = [NSJSONSerialization
            JSONObjectWithData:data options:kNilOptions error:&error1
        ];
        User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:[self managedObjectContext]];
        [user setValue:[innerJson objectForKey:@"token"] forKey:@"authToken"];
        [user setValue:_userName.stringValue forKey:@"email"];
        [user setValue:@true forKey:@"loggedIn"];
        
        NSError *error2 = nil;
        if ([[self managedObjectContext] save:&error2] == NO) {
            NSAssert(NO, @"Error saving context: %@\n%@", [error2 localizedDescription], [error2 userInfo]);
        }
        
        /////////////////////////////////////////////////////
        NSURL *URL1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://sleepy-crag-69713.herokuapp.com/search_urls?user_email=%@&user_token=%@", _userName.stringValue, [innerJson objectForKey:@"token"]]];
        NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:URL1];
        [request1 setHTTPMethod:@"GET"];
        error = nil;
        NSURLSessionConfiguration *config1 = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSDictionary *sessionHeaders1 = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         @"application/json", @"Accept", @"application/json",  @"Content-Type", nil];
        config1.HTTPAdditionalHeaders = sessionHeaders1;
        [request1 addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request1 addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        NSURLSession *session1 = [NSURLSession sessionWithConfiguration:config];
        NSURLSessionDataTask *uploadTask1 = [session1 dataTaskWithURL:URL1
                                                    completionHandler:^(NSData *data1,NSURLResponse *response1,NSError *error1)
        {
            /////////////////////////////////////////////////////
            // create the first parameter
            NSLog(@"dataAsString %@", [NSString stringWithUTF8String:[data1 bytes]]);
            NSMutableArray * innerJsonUrlsArray = [NSJSONSerialization
                                                   JSONObjectWithData:data1 options:kNilOptions error:&error1
                                                   ];
            for (NSMutableDictionary *innerJsonUrls in innerJsonUrlsArray)
            {
                Urls *urls = [NSEntityDescription insertNewObjectForEntityForName:@"Urls" inManagedObjectContext:[self managedObjectContext]];
                [urls setValue:[innerJsonUrls objectForKey:@"url"] forKey:@"urlString"];
                [urls setValue:[innerJsonUrls objectForKey:@"name"] forKey:@"name"];
                [urls setValue:user forKey:@"user"];
                //[urls setValue:[innerJsonUrls objectForKey:@"valid_till"] forKey:@"validTill"];
                NSError *error3 = nil;
                if ([[self managedObjectContext] save:&error3] == NO) {
                    NSAssert(NO, @"Error saving context: %@\n%@", [error3 localizedDescription], [error3 userInfo]);
                }
                //                                                                                        [user addUrlsObject:urls];
            }
            //_nextView = [[UrlListViewController alloc] initWithNibNameCustom:@"UrlListViewController" bundle:nil context: _managedObjectContext objectModel:_managedObjectModel storeCoordinator:_persistentStoreCoordinator user:user];
            [ _nextView setUser:user];
            [_statusItemPopup openUrlListView:_nextView];
            //[_statusItemPopup switchViews:_nextView];
            // call the event in AppleScript
        }];
        [uploadTask1 resume];
    }];
    [uploadTask resume];
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

@end
