//
//  User+CoreDataProperties.h
//  linkedNetworks
//
//  Created by Variable Vasas on 19/02/16.
//  Copyright © 2016 Variable Vasas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *authToken;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSNumber *loggedIn;
@property (nullable, nonatomic, retain) NSOrderedSet<Urls *> *urls;

@end

@interface User (CoreDataGeneratedAccessors)

- (void)insertObject:(Urls *)value inUrlsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromUrlsAtIndex:(NSUInteger)idx;
- (void)insertUrls:(NSArray<Urls *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeUrlsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInUrlsAtIndex:(NSUInteger)idx withObject:(Urls *)value;
- (void)replaceUrlsAtIndexes:(NSIndexSet *)indexes withUrls:(NSArray<Urls *> *)values;
- (void)addUrlsObject:(Urls *)value;
- (void)removeUrlsObject:(Urls *)value;
- (void)addUrls:(NSOrderedSet<Urls *> *)values;
- (void)removeUrls:(NSOrderedSet<Urls *> *)values;

@end

NS_ASSUME_NONNULL_END
