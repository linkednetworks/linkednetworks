//
//  User+CoreDataProperties.m
//  linkedNetworks
//
//  Created by Variable Vasas on 19/02/16.
//  Copyright © 2016 Variable Vasas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic authToken;
@dynamic email;
@dynamic id;
@dynamic loggedIn;
@dynamic urls;

@end
