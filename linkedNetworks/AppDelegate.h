//
//  AppDelegate.h
//  linkedNetworks
//
//  Created by Variable Vasas on 29/12/15.
//  Copyright © 2015 Variable Vasas. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AXStatusItemPopup.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>
- (void)initializePopOver;
- (void)initializeCoreData;
@property (strong, nonatomic) AXStatusItemPopup *statusItemPopup;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSViewController *nextView;
@end

